from pathlib import Path
from subprocess import run

import git

from gpstime import gpstime

from ._log import logger, CMDError
from . import _args
from ._report import Report
from . import _util


def git_global_config_set_safe_directory(path):
    """globally configure git to set path as safe directory"""
    paths = run(
        ['git', 'config', '--global', '--get-all', 'safe.directory'],
        capture_output=True,
        text=True,
    ).stdout.splitlines()
    if set(('*', str(path))).isdisjoint(paths):
        logger.warning("NOTE: Adding safe.directory exception for report path to user global git config.")  # noqa E501
        logger.warning("This is needed for to allow access to shared git repos.")
        logger.warning("To subvert future warnings, consider adding a blanket safe.directory exception:")  # noqa E501
        logger.warning("    git config --global --add safe.directory '*'")
        run(
            ['git', 'config', '--global', '--add',
             'safe.directory', path],
            check=True,
        )


def add_args(parser):
    _args.add_report_argument(parser)
    parser.add_argument(
        '--diff', '-d', action='store_true',
        help="show uncommited report diffs and exit"
    )
    parser.add_argument(
        '--valid', action='store_true',
        help="tag report version as 'valid' after committing"
    )
    parser.add_argument(
        '--message',
        help="commit message"
    )


def main(args):
    """commit report version

    This commits any changes to the report in to an internal git repo
    for tracking.  This must be done before exporting.

    """
    try:
        report = Report.find(args.report)
    except FileNotFoundError as e:
        raise CMDError(e)
    logger.info(f"report found: {report.path}")

    ##########
    # initial report git configuration
    #
    # always do this so that we make sure the repo has the most
    # up-to-date configuration

    # because the report directories are shared we need to make
    # exceptions for them in the users config
    # FIXME: we should definitely not be mucking with the users global
    # config settings, but i'm not sure how else to get around this
    # FIXME: should we do this globally with '*'? that seems extreme
    git_global_config_set_safe_directory(report.path)

    # this initializes the repo if it doesn't exist
    repo = git.Repo.init(report.path, initial_branch='main')

    # ignore all data files
    # FIXME: is this the right thing to do?  could use git lfs to
    # track *everything*, but that will more than double the size of
    # the report
    with open(Path(report.path, '.gitignore'), 'wt') as f:
        f.write('*~\n')
        f.write('measurements\n')
        f.write('*.xml\n')
        f.write('*.png\n')
        f.write('fir_plots\n')
    repo.index.add('.gitignore')

    # # git LFS setup
    # repo.git.lfs('install')
    # # purge old lfs config
    # Path(report.gen_path('.gitattributes')).unlink(missing_ok=True)
    # # FIXME: the measurement files should be specified as
    # # '--lockable', but we need a way to distinguish the chain hdf5
    # # files to be lockable because they might need to be updated
    # repo.git.lfs('track', '*.hdf5', '*.xml')
    # repo.git.lfs('track', '*.hdf5', '*.png', '*.json', '*.npz', '*.pdf')
    # # HACK: need to sort the .gitattributes file because the order is
    # # otherwise non-deterministic
    # gap = report.gen_path('.gitattributes')
    # with open(gap) as f:
    #     lines = f.readlines()
    # with open(gap, 'wt') as f:
    #     for line in sorted(lines):
    #         f.write(line)
    # repo.index.add('.gitattributes')

    ##########

    # add all files in the report to the index.
    # use the git command directly because for some reason it adds
    # cleaner and faster
    repo.git.add('*')

    if args.diff:
        p = run(
            ['git', 'diff', '--cached'],
            cwd=report.path,
        )
        raise SystemExit(p.returncode)

    else:
        print(repo.git.status())

    if repo.is_dirty():
        # this is kind of annoying, but invoke git directly because it
        # allows for the editor to pass through to the tty
        cmd = ['git', 'commit', '-a']
        if args.message:
            cmd += ['-m', args.message]
        p = run(
            cmd,
            cwd=report.path,
        )
        if p.returncode:
            raise SystemExit(p.returncode)

    if args.valid:
        for tag in repo.git.tag(points_at='HEAD').splitlines():
            if tag.startswith('valid-'):
                logger.info("this version of the report has already been tagged valid.")
                return
        gt = gpstime.parse("now")
        ts = _util.gen_timestamp(gt)
        tag = f"valid-{ts}"
        logger.info(f"git tagging report as valid: {tag}")
        repo.create_tag(
            tag,
            message=f"This report was marked valid on {gt}.",
        )
