from pathlib import Path
from subprocess import run

from ._log import logger, CMDError
from . import _args
from . import _util
from ._report import Report
from ._git import check_report_git_status


def add_args(parser):
    _args.add_report_argument(parser)
    _args.add_config_option(parser)


def main(args):
    """push report to archive

    """
    try:
        report = Report.find(args.report)
    except FileNotFoundError as e:
        raise CMDError(e)
    logger.info(f"report found: {report.path}")

    check_report_git_status(report)

    config = _util.load_config(args.config)

    local_path = Path(report.path)
    archive_location = Path(config['Common']['archive_location'], 'reports')

    logger.info(f"uploading report to archive: {archive_location}/{report.id}")

    cmd = ['rsync', '-avz', str(local_path), str(archive_location)]
    logger.debug(' '.join(cmd))
    run(cmd, check=True)

    # FIXME: git upload would be nicer, but it would take more work to
    # push to a repo with a checked out working tree
    # try:
    #     remote = repo.create_remote('archive', remote_url)
    # except git.exc.GitCommandError:
    #     remote = repo.remotes['archive']
    #     remote.set_url(remote_url)
    # remote.push()
