import git

from ._log import CMDError


def check_report_git_status(report, check_valid=False):
    try:
        repo = git.Repo(report.path)
    except git.exc.InvalidGitRepositoryError:
        raise CMDError("report is not a valid git repo, 'commit' report first.")

    if repo.is_dirty():
        print(repo.git.status())
        raise CMDError("report repo is dirty, 'commit' changes first.")

    if check_valid and not report.is_valid():
        raise CMDError("report has not been marked as valid, only valid reports can be exported.  report can be marked valid with 'commit --valid' command.")  # noqa E501


def git_hex2int(hexsha, digits=7):
    """convert git hexidecimal commit id into an integer.

    Take the first 'digits' of the hex and return an int.  The default
    'digits' is 7, which is a "standard" short git hex length that
    should be relatively unique, and is 28 bits, which fortunately
    fits into a standard EPICS 32-bit signed int.

    """
    return int(hexsha[:digits], 16)


def git_int2hex(intsha, digits=7):
    """convert integer into hex.

    The 'intsha' is converted to a hex string.  If hex is less than
    'digits' in length (default 7), the string is zero padded up to
    'digits'.

    """
    return hex(intsha)[2:].zfill(digits)
